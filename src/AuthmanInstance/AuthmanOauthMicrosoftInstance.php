<?php

declare(strict_types = 1);

namespace Drupal\authman_microsoft\AuthmanInstance;

use Drupal\authman\AuthmanInstance\AuthmanOauthInstance;
use Drupal\Core\Url;

/**
 * Adds Microsoft functionality to instance.
 */
class AuthmanOauthMicrosoftInstance extends AuthmanOauthInstance {

  /**
   * Unordered array of scopes.
   *
   * @var string[]
   */
  protected $scopes = [];

  /**
   * Sets scopes.
   *
   * @param array $scopes
   *   Unordered array of scopes.
   *
   * @return $this
   */
  public function setScopes(array $scopes) {
    $this->scopes = $scopes;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function authorizationCodeUrl(): Url {
    $options = [];
    if (!empty($this->scopes)) {
      $options['scope'] = $this->scopes;
    }
    return Url::fromUri($this->provider->getAuthorizationUrl($options));
  }

}
