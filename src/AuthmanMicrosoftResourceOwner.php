<?php

declare(strict_types=1);

namespace Drupal\authman_microsoft;

use Stevenmaguire\OAuth2\Client\Provider\MicrosoftResourceOwner;

/**
 * Wraps Microsoft resource owner.
 *
 * Remove after these PRs are committed:
 *  - https://github.com/stevenmaguire/oauth2-microsoft/pull/21
 *
 * This class should not be extended as it is considered a temporary fix.
 */
final class AuthmanMicrosoftResourceOwner extends MicrosoftResourceOwner {

  /**
   * {@inheritdoc}
   */
  public function getEmail() {
    return $this->response['mail'] ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrincipalName() {
    return $this->response['userPrincipalName'] ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getFirstname() {
    return $this->getGivenName();
  }

  /**
   * {@inheritdoc}
   */
  public function getGivenName() {
    return $this->response['givenName'] ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastname() {
    return $this->getSurname();
  }

  /**
   * {@inheritdoc}
   */
  public function getSurname() {
    return $this->response['surname'] ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->getDisplayName();
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplayName() {
    return $this->response['displayName'] ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function toArray() {
    return $this->response + [
      'first_name' => $this->response['givenName'],
      'last_name' => $this->response['surname'],
      'name' => $this->response['displayName'],
      'link' => NULL,
    ];
  }

}
