<?php

declare(strict_types = 1);

namespace Drupal\authman_microsoft;

use League\OAuth2\Client\Token\AccessToken;
use Stevenmaguire\OAuth2\Client\Provider\Microsoft;
use League\OAuth2\Client\Tool\BearerAuthorizationTrait;

/**
 * Wraps Microsoft provider so return values are valid.
 *
 * Remove after these PRs are committed:
 *  - https://github.com/stevenmaguire/oauth2-microsoft/pull/10
 *  - https://github.com/stevenmaguire/oauth2-microsoft/pull/21
 *
 * This class should not be extended as it is considered a temporary fix.
 */
final class AuthmanMicrosoftProvider extends Microsoft {

  use BearerAuthorizationTrait;

  /**
   * {@inheritdoc}
   */
  protected $urlResourceOwnerDetails = 'https://graph.microsoft.com/v1.0/me';

  /**
   * The access token type to use. Defaults to none.
   *
   * @var string
   */
  protected $accessTokenType = self::ACCESS_TOKEN_TYPE_NONE;

  /**
   * No access token type.
   *
   * @var string
   */
  public const ACCESS_TOKEN_TYPE_NONE = '';

  /**
   * Access token type 'Bearer'.
   *
   * @var string
   */
  public const ACCESS_TOKEN_TYPE_BEARER = 'Bearer';

  /**
   * {@inheritdoc}
   */
  protected function getAuthorizationHeaders($token = NULL): array {
    switch ($this->accessTokenType) {
      case self::ACCESS_TOKEN_TYPE_BEARER:
        return ['Authorization' => 'Bearer ' . $token];

      case self::ACCESS_TOKEN_TYPE_NONE:

      default:
        return [];
    }
  }

  /**
   * Sets the access token type used for authorization.
   *
   * @param string $accessTokenType
   *   The access token type to use.
   */
  public function setAccessTokenType(string $accessTokenType): void {
    $this->accessTokenType = $accessTokenType;
  }

  /**
   * {@inheritdoc}
   */
  protected function createResourceOwner(array $response, AccessToken $token) {
    return new AuthmanMicrosoftResourceOwner($response);
  }

}
