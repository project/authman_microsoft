<?php

declare(strict_types = 1);

namespace Drupal\authman_microsoft\Forms;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Form for configuring Microsoft plugins.
 *
 * @property \Drupal\authman_microsoft\Plugin\AuthmanOauth\AuthmanMicrosoft $plugin
 */
class AuthmanMicrosoftPluginForm extends PluginFormBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->plugin->getConfiguration();

    $scopes = implode("\r\n", $configuration['scopes']);
    $form['scopes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Scopes'),
      '#description' => $this->t('Scopes to request access. Account needs to be re-authenticated if these values are changed.'),
      '#default_value' => $scopes,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $scopes = array_filter(explode("\r\n", $form_state->getValue('scopes')));
    $configuration['scopes'] = $scopes;
    $this->plugin->setConfiguration($configuration);
  }

}
