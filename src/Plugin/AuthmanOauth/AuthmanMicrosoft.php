<?php

declare(strict_types = 1);

namespace Drupal\authman_microsoft\Plugin\AuthmanOauth;

use Drupal\authman\AuthmanInstance\AuthmanOauthInstanceInterface;
use Drupal\authman\Plugin\AuthmanOauth\AuthmanOauthPluginBase;
use Drupal\authman\Plugin\AuthmanOauth\AuthmanOauthPluginResourceOwnerInterface;
use Drupal\authman\Plugin\AuthmanOauth\AuthmanOauthPluginScopeInterface;
use Drupal\authman\Plugin\KeyType\OauthClientKeyType;
use Drupal\authman_microsoft\AuthmanInstance\AuthmanOauthMicrosoftInstance;
use Drupal\authman_microsoft\AuthmanMicrosoftProvider;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\key\KeyInterface;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use Stevenmaguire\OAuth2\Client\Provider\MicrosoftResourceOwner;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Microsoft OAuth provider.
 *
 * @AuthmanOauth(
 *   id = "authman_microsoft",
 *   label = @Translation("Microsoft"),
 *   grant_types = {
 *     \Drupal\authman\Entity\AuthmanAuthInterface::GRANT_AUTHORIZATION_CODE,
 *     \Drupal\authman\Entity\AuthmanAuthInterface::GRANT_REFRESH_TOKEN,
 *   },
 *   forms = {
 *     "configure" = "\Drupal\authman_microsoft\Forms\AuthmanMicrosoftPluginForm",
 *   }
 * )
 *
 * @internal
 */
class AuthmanMicrosoft extends AuthmanOauthPluginBase implements ConfigurableInterface, ContainerFactoryPluginInterface, AuthmanOauthPluginResourceOwnerInterface, AuthmanOauthPluginScopeInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->httpClient = $container->get('http_client');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'scopes' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance(array $providerOptions, string $grantType, KeyInterface $clientKey): AuthmanOauthInstanceInterface {
    $keyType = $clientKey->getKeyType();
    assert($keyType instanceof OauthClientKeyType);
    $provider = $this->createProvider($providerOptions, $clientKey);
    return (new AuthmanOauthMicrosoftInstance($provider, $grantType))
      ->setScopes($this->getConfiguration()['scopes']);
  }

  /**
   * {@inheritdoc}
   */
  protected function createProvider(array $providerOptions, $clientKey): AbstractProvider {
    $values = $clientKey->getKeyValues();
    $provider = new AuthmanMicrosoftProvider([
      'clientId' => $values['client_id'] ?? '',
      'clientSecret' => $values['client_secret'] ?? [],
    ] + $providerOptions);
    $provider->setHttpClient($this->httpClient);
    $provider->setAccessTokenType(AuthmanMicrosoftProvider::ACCESS_TOKEN_TYPE_BEARER);
    return $provider;
  }

  /**
   * {@inheritdoc}
   */
  public function renderResourceOwner(ResourceOwnerInterface $resourceOwner): array {
    assert($resourceOwner instanceof MicrosoftResourceOwner);
    $build = [];
    $build['owner'] = [
      '#theme' => 'authman_microsoft_resource_owner',
      'name' => $resourceOwner->getName(),
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getScopes(): array {
    return array_values($this->getConfiguration()['scopes'] ?? []);
  }

}
