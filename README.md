Authman Microsoft

https://www.drupal.org/project/authman_microsoft

Plugin implementation of Microsoft OAuth 2.0 for [Authman][authman]

[authman]: https://www.drupal.org/project/authman

Apps can be created at [Azure Portal][azure-portal]. See also
[Quickstart: Register an application with the Microsoft identity platform][azure-quickstart-register-app]

# License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# Example

Example from [Microsoft API][microsoft-api] to get emails for a user.

This example requires scope `Mail.Read`.

```php
/** @var \Drupal\authman\AuthmanInstance\AuthmanOauthFactoryInterface $factory */
$factory = \Drupal::service('authman.oauth');
$authmanInstance = $factory->get('foo');
try {
  $response = $authmanInstance->authenticatedRequest('GET', 'https://graph.microsoft.com/v1.0/me/messages');
  $successJson = \json_decode((string) $response->getBody());
}
catch (\GuzzleHttp\Exception\GuzzleException $e) {
  $errorJson = \json_decode((string) $e->getResponse()->getBody());
}
```

[azure-portal]: https://portal.azure.com/
[microsoft-api]: https://docs.microsoft.com/en-us/graph/api/user-list-messages
[azure-quickstart-register-app]: https://docs.microsoft.com/en-us/azure/active-directory/develop/quickstart-register-app
